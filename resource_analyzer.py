import subprocess, datetime, time, os
log_file = '/var/log/resource_analyzer.log'
flag = 0

def main():
    ##### LOG rotate function call
    global log_file
    global flag
    if os.path.isfile(log_file):
        log_rotate(log_file)
    LOAD_LIMIT = int(subprocess.Popen(['nproc'], stdout=subprocess.PIPE).communicate()[0].rstrip())
    IO_LIMIT = 40
    RAM_LIMIT = 95
    date = datetime.datetime.now().strftime("%d-%m-%Y %H:%M:%S")
    load = check_load()
    iowait  = check_io()
    ram = check_ram()
    ram_used = float(ram[1]) / float(ram[0]) * 100
    with open(log_file, "a") as log:
        log.write('{0} - Load: {1} , {2} , {3} \t IO Wait: {4} \t RAM Usage: {5:.2f}%\n'.format(date,load[0],load[1],load[2],iowait,ram_used))  

    if float(load[0]) > LOAD_LIMIT:
        run_load_commands()
        flag = 1
    if ram_used > RAM_LIMIT:
        run_ram_commands()
    if int(iowait) > IO_LIMIT:
        run_io_commands()

###Log rotate function
def log_rotate(log_file):
    size = os.path.getsize(log_file)
    limit = 52428800

    if size >= limit:
        log1 = '{0}.{1}'.format(log_file,'1')
        log2 = '{0}.{1}'.format(log_file,'2')
        log3 = '{0}.{1}'.format(log_file,'3')
        log4 = '{0}.{1}'.format(log_file,'4')

        if (os.path.isfile(log4)):
            os.remove(log4)
        if (os.path.isfile(log3)):
            os.rename(log3, log4)
        if (os.path.isfile(log2)):
            os.rename(log2, log3)
        if (os.path.isfile(log1)):
            os.rename(log1, log2)
        if (os.path.isfile(log_file)):
            os.rename(log_file, log1)

#####Check load function

def check_load():
    load_avg = subprocess.Popen(['cat', '/proc/loadavg'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].split()[:3]
    return(load_avg)

####Check RAM usagefunction

def check_ram():
    free = subprocess.Popen(['/usr/bin/free', '-m'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].decode('utf-8').split('\n')
    free_raw_2 = free[1].split()
    ram_info = free_raw_2[1:3]
    return(ram_info)

####Check I/O usage Function

def check_io():
    stdout = subprocess.Popen('vmstat 1 2', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True).communicate()[0]
    io_wait = stdout.rstrip().split('\n')[3].split()[stdout.rstrip().split('\n')[1].split().index('wa')]
    return io_wait
    
#### Print log file

def print_to_log(param, com_out):
    global log_file
    with open(log_file, "a") as log:
        log.write('\n\n {pat} {0} {pat} \n'.format(param.strip(), pat = '#'*35))
        cnt  = 1
        for item in com_out:
            if cnt % 2 == 0:
                log.write('\n {0} \n'.format(item))  
            else:
                log.write('\n {pat}  {0} {pat} \n'.format(item.strip(), pat = '*'*30))
            cnt += 1
        log.write('\n\n {pat} \n\n'.format(pat = '#'*80))


####Commands to run

def run_ram_commands():
    global flag
    if flag == 1:
        cmds = ['ps axo size,vsize,start_time,cmd,user,pmem | sort -rn']
    else:
        cmds = ['pstree', 'ps axo size,vsize,start_time,cmd,user,pmem | sort -rn', 'netstat -anp']
    flag += 2
    run('RAM', cmds)
    
def run_io_commands():
    global flag
    if flag == 0:
        cmds = ['pstree','top -bHc -n1','ps auwx','block_dump()']
    elif flag == 2:
        cmds = ['top -bHc -n1','ps auwx','block_dump()']
    else:
        cmds = ['ps auwx','block_dump()']
    run('IO', cmds)
    
def run_load_commands():
    cmds = ["top -bHc -n1", "top -bH -n1 |awk '{print $1}'|egrep -o '([0-9]{4,})'|head -n5|xargs -n1 lsof -p",'pstree', 'netstat -anp']
    if os.path.isfile('/usr/local/cpanel/version'):
        cmds.append('/usr/bin/lynx localhost/whm-server-status -dump -width 600 | grep -v OPTIONS')
    run('LOAD',cmds)

def block_dump():
    dm = """dmesg | egrep "READ|WRITE|dirtied" |cut -d']' -f2-|egrep -o '(.*)(READ|WRITE|dirtied)'| sort | uniq -c | sort -rn|head;echo 0 > /proc/sys/vm/block_dump"""
    if not subprocess.Popen('echo 1 > /proc/sys/vm/block_dump', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True).communicate()[1]:
        time.sleep(15) 
        dmesg = subprocess.Popen(dm, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE,universal_newlines=True).communicate()[0]
    else:
        dmesg = "Result not available - Permission denied on /proc/sys/vm/block_dump\n"
    return dmesg


#### RUN commands function
def run(param,cmds):
    cmd_names_and_output = []
    for cmd_name in cmds:
        if cmd_name.endswith('()'):
            cmd_out = eval(cmd_name)
            cmd_names_and_output.append(cmd_name)
            cmd_names_and_output.append(cmd_out)
        else:
            cmd_out = subprocess.Popen(cmd_name, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).communicate()[0].decode('utf-8')
            cmd_names_and_output.append(cmd_name)
            cmd_names_and_output.append(cmd_out)
    print_to_log(param,cmd_names_and_output)

main()
#Author : Jyothilakshmi.ku and Muhammed.Kv
#Version : 1.2
